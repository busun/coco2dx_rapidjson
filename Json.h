﻿#ifndef __JSON_H__
#define __JSON_H__

#include <string>
#include <vector>
#include <map>
#include <json/document.h>
#include <string.h>
using namespace std;

#define JsonValue	rapidjson::Value

class JsonArray;
class JsonObject
{
public:
    JsonObject();
    JsonObject(const string& str);
    JsonObject(const rapidjson::Value& value);
    ~JsonObject();
    void copyFrome(JsonObject& src);
    bool has(const string& key)const;
    //
    string getString(const string& key)const;
	int getInt(const string& key)const;
	const rapidjson::Value& getValue(const string& key)const;
    const rapidjson::Value& getObject(const string& key)const;
    const rapidjson::Value& getArray(const string& key)const;
    //
    void add(const string& key,const string& value);
    void add(const string& key,int value);
    void add(const string& key,JsonObject& value);
    void add(const string& key,JsonArray& value);
    string toString()const;
    rapidjson::Value& getRoot(){return m_document;};
    //序列化字符串
    static string jtoString(const rapidjson::Value& value);
    //深度拷贝函数
    static void addObject(rapidjson::Value &value,rapidjson::Value &root,rapidjson::Document &document);
    static void addArray(rapidjson::Value &value,rapidjson::Value &root,rapidjson::Document &document);
    static void addArrayValue(rapidjson::Value &value,rapidjson::Value &root,rapidjson::Document &document);
    static void addObjectValue(const string &key,rapidjson::Value &value,rapidjson::Value &root,rapidjson::Document &document);
    bool parse(const string& str);
private:
    rapidjson::Document     m_document;
};

class JsonArray
{
public:
    JsonArray();
    JsonArray(const string& str);
    JsonArray(const rapidjson::Value& value);
    ~JsonArray();
    void copyFrome(JsonArray& src);
    bool parse(const string& str);
    bool has(const string& key);
    //
    int count();
    rapidjson::Value& getVaue(const int index);
    //
    void add(const string& value);
    void add(int value);
    void add(JsonObject& value);
    void add(JsonArray& value);
    string toString();
    rapidjson::Value& getRoot(){return m_document;};
private:
    rapidjson::Document     m_document;
};
//


#endif //__JSON_H__
